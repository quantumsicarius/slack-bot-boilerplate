from slack import WebClient
import slack
import time
import requests

# Imports the Google Cloud client library
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

import os

# Add environment path.
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "LabsLEDTalk.json"
print("GOOGLE_APPLICATION_CREDENTIALS: " + os.environ["GOOGLE_APPLICATION_CREDENTIALS"])

# Instantiates a client
client = language.LanguageServiceClient()

slack_token = ""  # <- Enter your 'Bot User OAuth Access Token" here
rtmclient = slack.RTMClient(token=slack_token)
slack_client = slack.WebClient(token=slack_token)

def react_to_message(data, emoticon):
    print(
        slack_client.reactions_add()
    )

@slack.RTMClient.run_on(event='message')
def say_hello(**payload):
    data = payload['data']
    print(data)
    try:
        message_text = data["text"]

        # Google NLP Library stuff
        document = types.Document(
            content=message_text, type=enums.Document.Type.PLAIN_TEXT
        )

        # Detects the sentiment of the text
        sentiment = client.analyze_sentiment(
            document=document
        ).document_sentiment
        print(
            "The sentiment of: \"" + str(message_text) + "\" \nis:\n" + str(sentiment)
        )
        # Your code here
    except KeyError:
        pass

rtmclient.start()
